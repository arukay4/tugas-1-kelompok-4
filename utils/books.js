const fs = require('fs')

const getDataBooks = () => {
    if (!fs.existsSync('data/books.json')) {
        fs.writeFileSync('data/books.json', '[]', 'utf-8')
    }

    const fileBuffer = fs.readFileSync('data/books.json', 'utf-8')
    return JSON.parse(fileBuffer)
}

const findDataBooks = (id) => {
    const books = getDataBooks()

    return books.filter(book => {
        return book.id == id
    })


}

module.exports = { getDataBooks, findDataBooks }