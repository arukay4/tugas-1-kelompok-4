# Kelompok 4 - FSW 8

Academic task of Chapter 5. Authored by: Milda Aditia Putra, Muhammad Adi Wijaya, Hidayah Tria Ananda

CONTENTS OF THIS FILE
---------------------

 * Description
 * Requirements
 * Installation Guide
 * FAQ


## Description

This project is created to fulfill academic task of Chapter 5. In this project, we combine HTML, CSS, and Javascript (Bootstrap), Express JS framework, and View Engine using EJS. 

## Requirements
- Code Editor (VSCode, Sublime Text, etc.)
- [Bootstrap V-5](https://getbootstrap.com/)

## Installation Guide



## FAQ
For any questions and further informations, you can contact us through email.