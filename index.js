const exp = require('constants');
const express = require('express')
const {getDataBooks, findDataBooks} = require('./utils/books')
const app = express()
const port = 3000



//Set ejs
app.set('view engine', 'ejs');

app.use('/public', express.static('public'))

//Root 
app.get('/', (req, res) => {
    const books = getDataBooks()
    res.render('index', {
        title: 'OnLibrary | Home',
        books
    });
})

app.get('/register', (req, res) => {
    res.render('Register');
})

app.get('/buy/:id', (req, res) => {
    const book = findDataBooks(req.params.id)
    res.render('Buy', {
        title: `${(book[0]) ? 'Buy ' + book[0].title : '404'}`,
        book
    });
})

app.listen(port, () => {
    console.log(`server on di port ${port}`)
})